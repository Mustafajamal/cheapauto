<?php
/**
 * @package Insurance_Form
 * @version 1.0.0
 */
/*
Plugin Name: Insurance Form
Plugin URI: http://logicsbuffer.com/
Description: Insurance form Plugin shortcode [insform]
Author: Logics Buffer Team
Version: 1.0.0
Author URI: http://logicsbuffer.com/
*/

add_action( 'init', 'insurance_form' );

function insurance_form() {
	add_action( 'wp_enqueue_scripts', 'insurance_form_script' );
}
function insurance_form_script() {		        
	wp_enqueue_style( 'rt_custom_fancy_jqeury_ui', plugins_url().'/step-form/style.css',array(),time());
	wp_enqueue_script( 'insmainscript', plugins_url().'/step-form/script.js',array(),time());
	//wp_enqueue_script( 'isformjquery', plugins_url().'/step-form/jquery-3.6.0.min.js',array(),time());
	wp_enqueue_script( 'insphonemask', plugins_url().'/step-form/jquery-input-mask-phone-number.min.js',array(),time(),true);
}

// This just echoes the chosen line, we'll position it later.
function insuranceForm() {

	$ipaddress =  $_SERVER['REMOTE_ADDR'];
	function get_browser_name($user_agent)
	{
	    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
	    elseif (strpos($user_agent, 'Edge')) return 'Edge';
	    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
	    elseif (strpos($user_agent, 'Safari')) return 'Safari';
	    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
	    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
	   
	    return 'Other';
	}
	$user_browser = get_browser_name($_SERVER['HTTP_USER_AGENT']);
	$date = date('Y/m/d H:i:s');

ob_start();
//if(isset($_POST['submitdata'])){
	$zipcode = $_POST['zipcode'];
	$auto1year = $_POST['auto1-year'];
	$auto1make = $_POST['make'];
	$auto1model = $_POST['model'];
	//$auto1trim = $_POST['trim'];
	//$auto1ownership = $_POST['ownership'];
	$auto1primaryUse = $_POST['primaryUse'];
	//$auto1milesPerYear = $_POST['milesPerYear'];
	$auto1coverageType = $_POST['coverageType'];
	$universal_leadid = $_POST['universal_leadid'];
	echo $universal_leadid;

	$addAuto2 = $_POST['addAuto2'];
	$auto2year = $_POST['auto2-year'];
	$auto2make = $_POST['auto2make'];
	$auto2model = $_POST['auto2model'];
	//$auto2trim = $_POST['auto2-trim'];
	$auto2ownership = $_POST['auto2-ownership'];
	$auto2primaryUse = $_POST['auto2-primaryUse'];
	$auto2milesPerYear = $_POST['auto2-milesPerYear'];
	$auto2coverageType = $_POST['auto2-coverageType'];
	
	$hasInsurance = $_POST['hasInsurance'];
	$currentinsurer = $_POST['currentinsurer'];
	
	//echo $currentinsurer;

	$conthadautoins = $_POST['conthadautoins'];
	$howmanydrivers = $_POST['howmanydrivers'];
	$gender = $_POST['gender'];
	$married = $_POST['married'];
	//$educationlevel = $_POST['educationlevel'];
	$occupation = $_POST['occupation'];
	$atfaultaccidents = $_POST['atfaultaccidents'];
	$duiconviction = $_POST['duiconviction'];
	$ticketsyougot = $_POST['ticketsyougot'];
	$licsuspended = $_POST['licsuspended'];
	$ownhome = $_POST['ownhome'];
	$recpolquote = $_POST['recpolquote'];
	$servedmilitary = $_POST['servedmilitary'];
	$howwehelp = $_POST['howwehelp'];

	$dob_submit = $_POST['whatbdate'];
	$dob_2nddriv = $_POST['whatbdateseconddriv'];
	$dob_3rddriv = $_POST['whatbdatethirddriv'];
	$email_submit = $_POST['email'];
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$pnumber = $_POST['pnumber'];
	$address = $_POST['address'];
	$usertimezone = $_POST['usertimezone'];
	$bundleinsurance = $_POST['bundleinsurance'];

	//User Data
	$browserdata = $user_browser;
	$ipdata = $ipaddress;
	$usertimezone = $usertimezone;

	$howmanydrivers = $_POST['howmanydrivers'];
	$firstnamedriv2 = $_POST['firstnamedriv2'];
	$lastnamedriv2 = $_POST['lastnamedriv2'];
	$seconddrivgender = $_POST['2nddrivgender'];
	$seconddrivdui = $_POST['2nddrivdui'];
	$seconddrivticketsyougot = $_POST['2nddrivticketsyougot'];
	$seconddrivatfaultaccidents = $_POST['2nddrivatfaultaccidents'];
	$seconddrivlicsuspended = $_POST['2nddrivlicsuspended'];
	
	$firstnamedriv3 = $_POST['firstnamedriv3'];
	$lastnamedriv3 = $_POST['lastnamedriv3'];
	$thirddrivgender = $_POST['3rddrivgender'];
	$thirddrivdui = $_POST['3rddrivdui'];
	$thirddrivticketsyougot = $_POST['3rddrivticketsyougot'];
	$thirddrivatfaultaccidents = $_POST['3rddrivatfaultaccidents'];
	$thirddrivlicsuspended = $_POST['3rddrivlicsuspended'];

ob_start();
		
		//API Push
		// $curl = curl_init();
		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => 'http://124.109.39.133:7758/ords/ovacsol_api/api/ext_lead',
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => '',
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 0,
		//   CURLOPT_FOLLOWLOCATION => true,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => 'POST',
		//   CURLOPT_POSTFIELDS =>'{
		//    "leads":[
		//       {
		//          "leadsmaster":{
		//             "id":"1",
		//             "lead_id":"12",
		//             "time_stamp":"18-Jun-21 7:42:34 AM",
		//             "time_zone":"18-Jun-21 7:42:34 AM",
		//             "ip":"192.168.2.00",
		//             "browser":"ABC",
		//             "zip_code":"4545",
		//             "v1_year":"221",
		//             "v1_make":"Audi",
		//             "v1_model":"2020",
		//             "v1_coverage":"Nk",
		//             "v2_year":"3233",
		//             "v2_make":"32",
		//             "v2_model":"33",
		//             "past_auto_insurance":"fdf",
		//             "current_insurer":" dg",
		//             "current_insurer_tenure":" gg",
		//             "d1_gender":"gf",
		//             "d1_martial_status":"fg",
		//             "d1_fault_accidents":"gf",
		//             "d1_tickets":"fg",
		//             "d1_dui_conviction":"tr",
		//             "d1_license_suspended":"es",
		//             "d1_dob":"r",
		//             "d2_gender":"g ",
		//             "d2_martial_status":"t",
		//             "d2_fault_accidents":"y",
		//             "d2_tickets":"ew",
		//             "d2_dui_conviction":"bre",
		//             "d2_license_suspended":"er",
		//             "d2_dob":"bdt",
		//             "d3_martial_status":"btrt",
		//             "d3_fault_accidents":"brt",
		//             "d3_tickets":"fdf",
		//             "d3_dui_conviction":"rr",
		//             "d3_license_suspended":" t",
		//             "d3_dob":"td",
		//             "military_served":"bt",
		//             "insurance_help":"dtr",
		//             "rent_own":"vdrv",
		//             "quotes_rent_own":"dr",
		//             "bundle_insurance":"vr",
		//             "email":"ver",
		//             "street_address":"vfv",
		//             "phone_number":"fbtr"
		//          }
		//       }
		//    ]
		// }',
		//   CURLOPT_HTTPHEADER => array(
		//     "Content-Type: text/plain",
		//     "Cache-Control: no-cache",
		//     "Connection: keep-alive",
		//     "Content-Type: application/xml",
		//     "Host: 124.109.39.133:7758",
		//     "Postman-Token: ",
		//     "User-Agent: ",
		//     "accept-encoding: gzip, deflate",
		//     "cache-control: no-cache",
		//     "content-length: 1107"
		//   ),
		// ));

		// $response = curl_exec($curl);
		// $err = curl_error($curl);
		// curl_close($curl);
		// echo $response;
		// if ($err) {
		//   echo "cURL Error #:" . $err;
		// } else {
		//   echo $response;
		// }

	$url = 'http://124.109.39.133:7758/ords/ovacsol_api/api/ext_lead';
	$data = array('id' => '1', 'lead_id' => '12');

	// use key 'http' even if you send the request to https://...
	// $options = array(
	//     'http' => array(
	//         'header'  => "Content-type: application/x-www-form-urlencoded\r ",
	//         'method'  => 'POST',
	//         'content' => http_build_query($data)
	//     )
	// );
	// $context  = stream_context_create($options);
	// $result = file_get_contents($url, false, $context);
	// if ($result === FALSE) { /* Handle error */ }


	// $options = array(
	//   'http' => array(
	//     'header'  => "Content-type: application/x-www-form-urlencoded\r ",
	//     'method'  => 'POST',
	//     'content' => http_build_query($data),
	//   ),
	// );
	// $context  = stream_context_create($options);
	// $result = file_get_contents($url, false, $context);

	// var_dump($result);

	// $response = wp_remote_post( $url, array(
	//     'method'      => 'POST',
	//     'timeout'     => 45,
	//     'redirection' => 5,
	//     'httpversion' => '1.0',
	//     'blocking'    => true,
	//     'headers'     => array(),
	//     'body'        => array(
	//         'id' => '1',
	//         'lead_id' => '1234'
	//     ),
	//     'cookies'     => array()
	//     )
	// );
	 
	// if ( is_wp_error( $response ) ) {
	//     $error_message = $response->get_error_message();
	//     echo "Something went wrong: $error_message";
	// } else {
	//     echo 'Response:<pre>';
	//     print_r( $response );
	//     echo '</pre>';
	// }

	//print_r($response);


	require_once 'HTTP/Request2.php';
	$request = new HTTP_Request2();
	$request->setUrl('http://124.109.39.133:7758/ords/ovacsol_api/api/ext_lead');
	$request->setMethod(HTTP_Request2::METHOD_POST);
	$request->setConfig(array(
	  'follow_redirects' => TRUE
	));
	$request->setHeader(array(
	  'Content-Type' => 'application/json',
	  'Host' => '192.168.1.2:51331'

	));
	$request->setBody('{
	   "leads":[
	      {
	          "leadsmaster":{
	             "id":"1",
	             "lead_id":"12",
	             "time_stamp":"18-Jun-21 7:42:34 AM",
	             "time_zone":"18-Jun-21 7:42:34 AM",
	             "ip":"192.168.2.00",
	             "browser":"ABC",
	             "zip_code":"4545",
	             "v1_year":"221",
	             "v1_make":"Audi",
	             "v1_model":"2020",
	             "v1_coverage":"Nk",
	             "v2_year":"3233",
	             "v2_make":"32",
	             "v2_model":"33",
	             "past_auto_insurance":"fdf",
	             "current_insurer":" dg",
	             "current_insurer_tenure":" gg",
	             "d1_gender":"gf",
	             "d1_martial_status":"fg",
	             "d1_fault_accidents":"gf",
	             "d1_tickets":"fg",
	             "d1_dui_conviction":"tr",
	             "d1_license_suspended":"es",
	             "d1_dob":"r",
	             "d2_gender":"g ",
	             "d2_martial_status":"t",
	             "d2_fault_accidents":"y",
	             "d2_tickets":"ew",
	             "d2_dui_conviction":"bre",
	             "d2_license_suspended":"er",
	             "d2_dob":"bdt",
	             "d3_martial_status":"btrt",
	             "d3_fault_accidents":"brt",
	             "d3_tickets":"fdf",
	             "d3_dui_conviction":"rr",
	             "d3_license_suspended":" t",
	             "d3_dob":"td",
	             "military_served":"bt",
	             "insurance_help":"dtr",
	             "rent_own":"vdrv",
	             "quotes_rent_own":"dr",
	             "bundle_insurance":"vr",
	             "email":"ver",
	             "url":"www.com",
	             "street_address":"vfv",
	             "phone_number":"fbtr"
	          }
	       }
	    ]
	 }');
	try {
	  $response = $request->send();
	  if ($response->getStatus() == 200) {
	    echo $response->getBody();
	  }
	  else {
	    echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
	    $response->getReasonPhrase();
	  }
	}
	catch(HTTP_Request2_Exception $e) {
	  echo 'Error: ' . $e->getMessage();
	}





		//API Push Ends


	echo"<h2 style='color:green;text-align:center;margin:50px;'>Your data has been Submitted! local agent will get back to you.</h2>";
	?>
	<style type="text/css">
		.insurance-form.form-main{display: none;}
		.getotherquote{display: block !important;}
	</style>
	<?php
//}
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    var tz = jstz.determine(); // Determines the time zone of the browser client
    //console.log(tz);
    var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
    //console.log(timezone);
    document.getElementById("usertimezone").value = timezone;
   }); 
 </script>
 	<div class="container getotherquote" style="display: none;">
		<div class="row">
			<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
			<div class="col-12 col-sm-8 col-md-8 col-lg-8 anotherqinner">
				<input type="button" href="https://AutoInsurancein.org/" id="getotherq" name="getotherq" class="btn-lg btn-block btn btn-primary" value="Get Another Quote">
			</div>
			<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
		</div>			
	</div>				
	
	<div class="insurance-form form-main">
		<form action="" method="post" name="insurance-form">				
			<div class="step1 stepmain firststep" step-val="1">
				<h2 class="sc-8uykmr-1 formtitlestep1" style="color: white;">Free Auto Insurance Comparison</h2>
				<p style="text-align: center;color: white;">Looking to save on cheap auto insurance? Compare quotes from the top auto insurance companies and save up to $720.</p>
				<br>
				<div class="container">
					<div class="row">
						<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
						<div class="col-12 col-sm-8 col-md-8 col-lg-8 zipcode_parent">
							<div class="form-group">
							  	<label for="usr">What is your ZIP Code?</label>
							  	<input type="text" class="form-control" data-val="1" name="zipcode" id="zipcode">
							  	<input type="hidden" class="form-control" data-val="1" name="json_response" id="json_response">
							  	<input type="hidden" class="form-control" name="usertimezone" id="usertimezone">
							</div>
							<button type="button" id="step1_submit" data-val="1" class="btn-lg btn-block btn btn-primary">Continue</button>
						</div>
						<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
					</div>
				</div>
			</div>
			
			<!--<div class="step10 stepmain hedethisstep" step-val="10">
				<div class="container">
					<div class="row">
						<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
						<div class="col-12 col-sm-8 col-md-8 col-lg-8">
								<h2 class="qhead" style="text-align:center;">Do you own or lease your <span class="selected_car2"></span>?</h2>
							<div class="kho8nb-0 jDiNWS">
								<input type="radio" id="auto2-ownership-OWN-OWNED" data-val="10" name="auto2-ownership" class="pbtpt4-1 kBNAEq" value="OWN_OWNED">
								<label for="auto2-ownership-OWN-OWNED" class="pbtpt4-0 eSQqD fcerNr">Owned</label>
								<input type="radio" id="auto2-ownership-OWN-FINANCED" data-val="10" name="auto2-ownership" class="pbtpt4-1 kBNAEq" value="OWN_FINANCED">
								<label for="auto2-ownership-OWN-FINANCED" class="pbtpt4-0 eSQqD fcerNr">Financed</label>
								<input type="radio" id="auto2-ownership-OWN-LEASED" data-val="10" name="auto2-ownership" class="pbtpt4-1 kBNAEq" value="OWN_LEASED">
								<label for="auto2-ownership-OWN-LEASED" class="pbtpt4-0 eSQqD fcerNr">Leased</label>
							</div>
						</div>
						<div class="col-1 col-sm-2 col-md-2 col-lg-2"></div>
					</div>
				</div>
			</div>-->
			
			<div class="step41 stepmain" step-val="41">
				<div class="container">
					<h2 style="text-align:center;">What is your street address?</h2>
					<div class="row">
						<div class="col-1 col-sm-4 col-md-4 col-lg-4"></div>
						<div class="col-12 col-sm-4 col-md-4 col-lg-4">														
							<div class="form-group">
								<input type="text" aria-expanded="false" autocomplete="off" data-val="41" id="address" maxlength="64" name="address" placeholder="e.g. 1 Main Street" class="sc-1j6xa08-0 bAiYtV" value="">
							</div>
							<input type="button" id="addresssubmit" value="Continue" data-val="41" class="btn-lg btn-block btn btn-primary">
						</div>
						<div class="col-1 col-sm-4 col-md-4 col-lg-4"></div>
					</div>
				</div>
			</div>
			<div class="step42 stepmain laststep " step-val="42">
				<div class="container">
					<h2 style="text-align:center;">Final Step! What is your phone number?</h2>
					<div class="row">
						<div class="col-1 col-sm-4 col-md-4 col-lg-4"></div>
						<div class="col-12 col-sm-4 col-md-4 col-lg-4">														
							<div class="form-group">
								<label for="phone">Phone</label>
								<input type="tel" id="phone" data-val="42" name="pnumber" placeholder="(212) 454-5454" class="form-control">
								<span id="spnPhoneStatus"></span>
							</div>
							<input id="leadid_token" name="universal_leadid" type="hidden" value=""/>
							<input type="submit" id="submitdata" name="submitdata" class="btn-lg btn-block btn btn-primary" value="Get Quotes">				
							<input type="hidden" id="total_steps" name="total_steps" value="">
											
						</div>
						<div class="col-1 col-sm-4 col-md-4 col-lg-4"></div>
					</div>
					<p style="padding: 2% 10%;font-size: 12px;">By clicking Get Quotes and submitting this form, I am providing express written consent to being contacted by you, Quoted Leads (QL) partnered agents, Farmers Insurance, Nationwide Insurance, Liberty Mutual Insurance Company, State Farm, Allstate Insurance Company, Ahern Agency or by one or more agents or brokers of your partners which companies I agree may reach me to discuss my interest, including offers of insurance, at the phone number and/or email address I have provided to you in submitting this form and/or additional information obtained. I consent by electronic signature to being contacted by telephone (via call and/or text) for marketing/telemarketing purposes at the phone number I provided in this form, even if my phone number is listed on a Do Not Call Registry, and I agree that such contact may be made using an automatic telephone dialing system and/or an artificial or prerecorded voice (standard call, text message, and data rates apply). I can revoke my consent at any time. I also understand that my agreement to be contacted is not a condition of purchasing any property, goods or services, and that I may call 1-219-227-2331 to speak with someone about obtaining an insurance quote. By clicking Show My Quotes and submitting this form, I affirm that I have read and agree to this website’s Privacy Policy and Terms of Use, including the arbitration provision and the E-SIGN Consent.</p>
				</div>
			</div>
			<div class="step43 stepmain emptycount" step-val="43"></div>
			<input type="hidden" id="leadid_tcpa_disclosure" /><label style="visibility: hidden;height: 10px;" for="leadid_tcpa_disclosure">By clicking Get Quotes and submitting this form, I am providing express written consent to being contacted by you, Quoted Leads (QL) partnered agents, Farmers Insurance, Nationwide Insurance, Liberty Mutual Insurance Company, State Farm, Allstate Insurance Company, Ahern Agency or by one or more agents or brokers of your partners which companies I agree may reach me to discuss my interest, including offers of insurance, at the phone number and/or email address I have provided to you in submitting this form and/or additional information obtained. I consent by electronic signature to being contacted by telephone (via call and/or text) for marketing/telemarketing purposes at the phone number I provided in this form, even if my phone number is listed on a Do Not Call Registry, and I agree that such contact may be made using an automatic telephone dialing system and/or an artificial or prerecorded voice (standard call, text message, and data rates apply). I can revoke my consent at any time. I also understand that my agreement to be contacted is not a condition of purchasing any property, goods or services, and that I may call 1-219-227-2331 to speak with someone about obtaining an insurance quote. By clicking Show Get Quotes and submitting this form, I affirm that I have read and agree to this website’s Privacy Policy and Terms of Use, including the arbitration provision and the E-SIGN Consent.</label>		
		</form>
	</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUNWfq7jUprmdjqsEIgnUaTtGbmjgfYXw&libraries=places"></script>
<?php

    // return the buffer contents and delete
    return ob_get_clean();
}

// Now we set that function up to execute when the admin_notices action is called.
add_shortcode( 'insform', 'insuranceForm' );
