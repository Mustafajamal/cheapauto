jQuery( document ).ready(function() {

		jQuery('#pnumbersubmit').hide();
		jQuery(".form-questions-header").hide();
		//Hide all steps be default
		//Loop on each steps
		jQuery( ".stepmain" ).each(function( index ) {
			var firststep = 1;
			jQuery(this).hide();
			var current_index = jQuery(this).attr('step-val');
			if (current_index == 1){
		  		jQuery(this).show();
		  	}
		});

		//On Enter From Submit Restrick
		jQuery(window).keydown(function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		//Previous Years Check
		jQuery("#prevsyearsparent").hide();
		jQuery('#previousyearschk').change(function() {
	        ////console.log("prev years check");
	        //if(this.checked) {
	        	jQuery("#prevsyearsparent").toggle();
	        //}	
    	});
    	//Other car Makes
		jQuery("#othermakesparent").hide();
		jQuery('#othermakeschk').change(function() {
	        ////console.log("prev years check");
	        //if(this.checked) {
	        	jQuery("#othermakesparent").toggle();
	        //}	
    	});
    	//Other car Makes 2
		jQuery("#othermakes2parent").hide();
		jQuery('#othermakes2chk').change(function() {
	        ////console.log("prev years check");
	        //if(this.checked) {
	        	jQuery("#othermakes2parent").toggle();
	        //}	
    	});

	    jQuery("button#step1_submit").click(function(){
	    	jQuery(".step1.stepmain").hide();
	    	jQuery(".step2.stepmain,.form-questions-header").show();
	    });
	    
	    jQuery(".insurance-form.form-main input:radio,#dob_submit,#email_submit,#name_submit,#pnumbersubmit").on('click',function(){		  
			var secondautosel = jQuery('input[name="addAuto2"]:checked').val();
			var current_index;
			var total_steps;
			var current_step;
			////console.log("secondautosel.."+secondautosel);
			////console.log(jQuery(this).attr('name'));

		    jQuery("#appostrophy").hide();
		    jQuery('input[name="ownhome"]').click(function() {
		    	var home_ownership = jQuery('input[name="ownhome"]:checked').val();
		    	jQuery("#home_ownership").html(home_ownership);
		    	if (home_ownership == "own") {
		    		jQuery("#home_ownership").html("home");
		    	}if (home_ownership == "rent") {
		    		jQuery("#home_ownership").html("renter");
		    		jQuery("#appostrophy").show();
		    	}
		    });
		    //Auto Make 1	
		    jQuery('input[name="make"]').click(function() {			
		    	//Get selected Car
			    var selected_car = jQuery('input[name="make"]:checked').val();
			    ////console.log("selected_car..."+selected_car);
			    jQuery(".selected_car").each(function( index ) {
			    	jQuery(this).html(selected_car);
			    });

			    var selected_car = jQuery('input[name="make"]:checked').val();
			    jQuery(".selcarmodel").each(function() {
			    	var selected_model = jQuery(this).attr('data-val');
			    	////console.log("selected_model..."+selected_model);

			    	if (selected_model == selected_car.toLowerCase()){
			    		jQuery(this).show();
			    	}
			    	else{
			    		jQuery(this).hide();
			    	}
			    });
		    });
		    //Auto Make 2	
		    jQuery('input[name="auto2make"]').click(function() {			
		    	//Get selected Car
			    var selected_car2 = jQuery('input[name="auto2make"]:checked').val();
			    ////console.log("selected_car2..."+selected_car2);
			    jQuery(".selected_car2").each(function( index ) {
			    	jQuery(this).html(selected_car2);
			    });

			    var selected_car2 = jQuery('input[name="auto2make"]:checked').val();
			    jQuery(".selcarauto2model").each(function() {
			    	var selected_model2 = jQuery(this).attr('data-val');
			    	////console.log("selected_model2..."+selected_model2);

			    	if (selected_model2 == selected_car2.toLowerCase()){
			    		jQuery(this).show();
			    	}
			    	else{
			    		jQuery(this).hide();
			    	}
			    });
		    });
		    //Number of drivers DOB
		    jQuery("#dobseconddriv").hide();
    		jQuery("#dobfirstdriv").show();
    		jQuery("#dobthirddriv").hide();
    		jQuery('input[name="howmanydrivers"]').click(function() {
		    	var selected_drivers = jQuery(this).val();
		    	if (selected_drivers == 1){
		    		// jQuery("#dobfirstdriv").show();
		    		// jQuery("#dobseconddriv").hide();
		    		// jQuery("#dobthirddriv").hide();
		    		jQuery("#seconddrivername").hide();
		    		jQuery("#thirddrivername").hide();
		    		jQuery("#1driv_submit").on('click',function(){
		    			jQuery("#dob_submit").trigger("click");
		    		});
		    	}
		    	if (selected_drivers == 2){
		    		jQuery("#dobseconddriv").show();
		    		jQuery("#dobfirstdriv").show();
		    		jQuery("#dobthirddriv").hide();

		    		jQuery("#seconddrivername").show();
		    		jQuery("#thirddrivername").hide();

		    		//Inner db steps
		    		jQuery("#1driv_submit").on('click',function(){
		    			jQuery("#dobfirstdriver").hide();
		    			jQuery("#dobseconddriv").show();
		    		});
		    		jQuery("#2driv_submit").on('click',function(){
		    			jQuery("#dob_submit").trigger("click");
		    		});
		    	}
		    	if (selected_drivers == 3){
		    		jQuery("#dobthirddriv").show();
		    		jQuery("#dobseconddriv").show();
		    		jQuery("#dobfirstdriv").show();
		    		jQuery("#seconddrivername").show();
		    		jQuery("#thirddrivername").show();

		    		//Inner db steps
		    		jQuery("#1driv_submit").on('click',function(){
		    			jQuery("#dobfirstdriver").hide();
		    			jQuery("#dobseconddriv").show();
		    		});
		    		jQuery("#2driv_submit").on('click',function(){
		    			jQuery("#dobfirstdriver").hide();
		    			jQuery("#dobseconddriv").hide();
		    			jQuery("#dobthirddriv").show();
		    		});
		    		jQuery("#3driv_submit").on('click',function(){
		    			jQuery("#dob_submit").trigger("click");
		    		});
		    	}
		    });

    		var selected_drivers = jQuery('input[name="howmanydrivers"]:checked').val();
    		var hasInsurance = jQuery('input[name="hasInsurance"]:checked').val();
			if(jQuery(this).attr('name') == "addAuto2" ) {
			  if(secondautosel == "false") {
		        ////console.log("com inside");
		        //return false; // breaks
		        current_step = jQuery(".step2ndautofalse").attr('step-val');
		        //
		        current_step = parseInt(current_step) - parseInt(1);
		        var next_step = parseInt(current_step) + parseInt(1);
		        localStorage.setItem("next_step", next_step);
		        localStorage.setItem("current_step", current_step);
			  }
			  else if(secondautosel == "true") {
				current_step = jQuery(this).attr('data-val');
				localStorage.setItem("current_index", current_step);
				var next_step = parseInt(current_step) + parseInt(1);
				localStorage.setItem("next_step", next_step);
				localStorage.setItem("current_step", current_step);
			  }
			}
			else if(selected_drivers == 1 && jQuery(this).attr('step-name') == "dob") {
				////console.log("falling in 1 and DOB");
				current_step = jQuery(this).attr('data-val');
				localStorage.setItem("current_step", current_step);
				localStorage.setItem("current_index", current_step);
				var next_step = parseInt(current_step) + parseInt(11);
				localStorage.setItem("next_step", next_step);
			}
			else if(selected_drivers == 2 && jQuery(this).attr('step-name') == "dob2drivers") {
				////console.log("falling in 2 and dob2drivers");
				current_step = jQuery(this).attr('data-val');
				localStorage.setItem("current_step", current_step);
				localStorage.setItem("current_index", current_step);
				var next_step = parseInt(current_step) + parseInt(7);
				localStorage.setItem("next_step", next_step);
			}
			//Hide milage steps			
			else if(secondautosel == "true" && jQuery(this).attr('name') == "auto2model") {
				////console.log("Falling in removing Lease");
				current_step = jQuery(this).attr('data-val');
				localStorage.setItem("current_step", current_step);
				localStorage.setItem("current_index", current_step);
				var next_step = parseInt(current_step) + parseInt(3);
				localStorage.setItem("next_step", next_step);
			}
			// else if(selected_drivers == 3 && jQuery(this).attr('step-name') == "dob") {
			// 	//console.log("falling in 3 and dob3drivers");
			// 	current_step = jQuery(this).attr('data-val');
			// 	//console.log("current_step.."+current_step);
			// 	localStorage.setItem("current_step", current_step);
			// 	localStorage.setItem("current_index", current_step);
			// 	var next_step = parseInt(current_step) + parseInt(1);
			// 	localStorage.setItem("next_step", next_step);
			// }
			//Have you had auto insurance in the past 30 days?
			else if(hasInsurance == "no" && jQuery(this).attr('step-name') == "hadins") {
				current_step = jQuery(this).attr('data-val');
				localStorage.setItem("current_step", current_step);
				localStorage.setItem("current_index", current_step);
				var next_step = parseInt(current_step) + parseInt(3);
				localStorage.setItem("next_step", next_step);
			}
			else{
				current_step = jQuery(this).attr('data-val');
				localStorage.setItem("current_step", current_step);
				localStorage.setItem("current_index", current_step);
				var next_step = parseInt(current_step) + parseInt(1);
				localStorage.setItem("next_step", next_step);
			}

			//Loop on each steps
			var arr = jQuery('.stepmain');
			jQuery(".stepmain").each(function( index ) {
			  	jQuery(this).hide();
			
			  	// var current_index = jQuery(this).attr('step-val');
			  	var next_step = localStorage.getItem("next_step");
				jQuery('[step-val='+next_step+']').show();
				
				  	//if(jQuery(this).is(':last-child')){
				  	if(index == arr.length - 1){
				  		var total_steps = index;
				  		//localStorage.setItem("total_steps", total_steps);
				  		//console.log("falling in last index");
				  		jQuery("#total_steps").val(total_steps);
				  		document.getElementById("total_steps").value = total_steps;
				  	} 
			  	 	
			  	if (current_index == next_step){
			  		jQuery(this).show();
			  	}
		  	});	    		
			current_index = localStorage.getItem("current_index");
			//total_steps = localStorage.getItem("total_steps");
			var total_steps = jQuery("#total_steps").val();


			current_step = localStorage.getItem("current_step");
			next_step = localStorage.getItem("next_step");
			var percentage = parseInt(current_index) * parseInt(100) / parseInt(total_steps);
			
			////console.log("total_steps..."+total_steps);
			////console.log("percentage.."+percentage);
			////console.log("current_step.."+current_step);
			////console.log("next_step.."+next_step);
			
			jQuery("#percentageval").html(percentage.toFixed(0));
			jQuery("rect#progressfill").attr('width',percentage.toFixed(0)+'%');
		});

	    //Address Validation
	    jQuery("#addresssubmit").hide();
	    jQuery("#address").on("keyup change", function(e) {
		    var address = jQuery("#address").val();
		    if (address == "") {
		    	jQuery("#addresssubmit").hide();
		    }else{
		    	jQuery("#addresssubmit").show();
		    }
		});	

		//Name Validation
		jQuery("input:radio").on('click',function(){
		    var selected_drivers = jQuery('input[name="howmanydrivers"]:checked').val();
		    if(selected_drivers == 1){
			    jQuery("#name_submit").hide();
			}
			if(selected_drivers == 2){
			    jQuery("#name_submit").hide();
			}
			if(selected_drivers == 3){
			    jQuery("#name_submit").hide();
			} 
		}); 
		    jQuery("#firstname,#lastname,#firstnamedriv2,#lastnamedriv2,#firstnamedriv3,#lastnamedriv3").on("keyup change", function(e) {
			    var selected_drivers = jQuery('input[name="howmanydrivers"]:checked').val();
			    if(selected_drivers == 1){
				    var firstname = jQuery("#firstname").val();
				    var lastname = jQuery("#lastname").val();
				    if (firstname == "" || lastname == "" ) {
				    	jQuery("#name_submit").hide();
				    }else{
				    	jQuery("#name_submit").show();
				    }
				}
				if(selected_drivers == 2){
					var firstnamedriv2 = jQuery("#firstnamedriv2").val();
				    var lastnamedriv2 = jQuery("#lastnamedriv2").val();
				    if (firstnamedriv2 == "" || lastnamedriv2 == "" ) {
				    	jQuery("#name_submit").hide();
				    }else{
				    	jQuery("#name_submit").show();
				    }
				}
				if(selected_drivers == 3){
					var firstnamedriv3 = jQuery("#firstnamedriv3").val();
				    var lastnamedriv3 = jQuery("#lastnamedriv3").val();
				    if (firstnamedriv3 == "" || lastnamedriv3 == "" ) {
				    	jQuery("#name_submit").hide();
				    }else{
				    	jQuery("#name_submit").show();
				    }
				}
			});	    		    	    
		 //    jQuery("#firstnamedriv2,#lastnamedriv2").on("keyup change", function(e) {
			//     var firstnamedriv2 = jQuery("#firstnamedriv2").val();
			//     var lastnamedriv2 = jQuery("#lastnamedriv2").val();
			//     if (firstnamedriv2 == "" || lastnamedriv2 == "" ) {
			//     	jQuery("#name_submit").hide();
			//     }else{
			//     	jQuery("#name_submit").show();
			//     }
			// });	    		           
		 //    jQuery("#firstnamedriv3,#lastnamedriv3").on("keyup change", function(e) {
			//     var firstnamedriv3 = jQuery("#firstnamedriv3").val();
			//     var lastnamedriv3 = jQuery("#lastnamedriv3").val();
			//     if (firstnamedriv3 == "" || lastnamedriv3 == "" ) {
			//     	jQuery("#name_submit").hide();
			//     }else{
			//     	jQuery("#name_submit").show();
			//     }
			// });	    	
	   
	    //Email Validation	    	
	    function validateEmail($email) {
		  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  return emailReg.test( $email );
		}
		//By default disable email submit
		jQuery("#email_submit").prop('disabled', true);
		
		jQuery("#email").on("keyup change", function(e) {
			var emailaddress = jQuery("#email").val();
			if( !validateEmail(emailaddress)) { 
				jQuery("#email_submit").prop('disabled', true);
			}else{
				jQuery("#email_submit").prop('disabled', false);
			}
		});	
		
		//Datepicker split
		jQuery("#birthMonth,#birthDay,#birthYear").on("keyup change", function(e) {
			var birthMonth = jQuery("#birthMonth option:selected").val();
			var birthDay = jQuery('#birthDay').val();
			var birthYear = jQuery('#birthYear').val();
			var setdate = birthMonth+"-"+("0"+birthDay).slice(-2)+"-"+(birthYear);
			jQuery("#birthdate").val(setdate);
		});
		//Second Driver
		jQuery("#birthMonth2,#birthDay2,#birthYear2").on("keyup change", function(e) {
			var birthMonth2 = jQuery("#birthMonth2 option:selected").val();
			var birthDay2 = jQuery('#birthDay2').val();
			var birthYear2 = jQuery('#birthYear2').val();
			var setdate2 = birthMonth2+"-"+("0"+birthDay2).slice(-2)+"-"+(birthYear2);
			jQuery("#whatbdateseconddriv").val(setdate2);
		});
		//Third Driver
		jQuery("#birthMonth3,#birthDay3,#birthYear3").on("keyup change", function(e) {
			var birthMonth3 = jQuery("#birthMonth3 option:selected").val();
			var birthDay3 = jQuery('#birthDay3').val();
			var birthYear3 = jQuery('#birthYear3').val();
			var setdate3 = birthMonth3+"-"+("0"+birthDay3).slice(-2)+"-"+(birthYear3);
			jQuery("#whatbdatethirddriv").val(setdate3);
		});
		
		//Zipcode Validation
		jQuery("#step1_submit").prop('disabled', true);
		jQuery('#zipcode').on('click',function(){
			var zipcode = jQuery("#zipcode").val();
			if (zipcode === '' || zipcode === null) {
				jQuery("#step1_submit").prop('disabled', true);
			}else{
				jQuery("#step1_submit").prop('disabled', false);
			}
		});	
		//Check empty DOB		
		jQuery("#invaliddate1").hide();
		jQuery("#invaliddate2").hide();
		jQuery("#invaliddate3").hide();

		jQuery('#1driv_submit').hide();
		jQuery('#2driv_submit').hide();
		jQuery('#3driv_submit').hide();
		
		//Limit length
		jQuery('#birthDay,#birthDay2,#birthDay3').on('keyup change',function(){
		    var max_chars = 2;
		    var max_value = 31;
		    if (jQuery(this).val().length >= max_chars) { 
		        jQuery(this).val(jQuery(this).val().substr(0, max_chars));
		    }
		    if (jQuery(this).val() > max_value) { 
		        jQuery(this).val(max_value);
		    }
		});
		jQuery('#birthYear,#birthYear2,#birthYear3').on('keyup change focus keypress',function(){
		    var max_chars = 4;
		    var max_value = 2021;
		    if (jQuery(this).val().length > max_chars) { 
		        jQuery(this).val().substr(0, max_chars);
		    }
		    if (jQuery(this).val() >= max_value) { 
		        jQuery(this).val(max_value);
		    }
		});
		//Check empty DOB fields First Driv 
		jQuery('#birthYear,#birthDay,#birthMonth').on('click change keyup',function(){
			
			//var valueDate = document.getElementById('birthdate').value;
			var birthMonth = jQuery("#birthMonth").val();
			var birthDay = document.getElementById('birthDay').value;
			var birthYear = document.getElementById('birthYear').value;
			
			if ( birthMonth == 0 || birthDay == "" || birthYear == "" || birthYear == null ){
			    ////console.log("Inside Check");
			    jQuery("#invaliddate1").show();
			    jQuery('#1driv_submit').hide();
			    document.getElementById("1driv_submit").style.display = 'none';
			    //return false;
			}else{
				jQuery("#invaliddate1").hide();
				jQuery('#1driv_submit').show();
			}
		});
		//Check empty DOB fields Second Driv 
		jQuery('#birthYear2,#birthDay2,#birthMonth2').on('click change keyup',function(){
			
			//var valueDate = document.getElementById('birthdate').value;
			var birthMonth2 = jQuery("#birthMonth2").val();
			var birthDay2 = document.getElementById('birthDay2').value;
			var birthYear2 = document.getElementById('birthYear2').value;

			if ( birthMonth2 == 0 || birthDay2 == "" || birthYear2 == "" || birthYear2 == null ){
			    //console.log("Inside Check");
			    jQuery("#invaliddate2").show();
			    jQuery('#2driv_submit').hide();
			    document.getElementById("1driv_submit").style.display = 'none';
			    //return false;
			}else{
				jQuery("#invaliddate2").hide();
				jQuery('#2driv_submit').show();
			}
		});
		//Check empty DOB fields Third Driv 
		jQuery('#birthYear3,#birthDay3,#birthMonth3').on('click change keyup',function(){
			
			//var valueDate = document.getElementById('birthdate').value;
			var birthMonth3 = jQuery("#birthMonth3").val();
			var birthDay3 = document.getElementById('birthDay3').value;
			var birthYear3 = document.getElementById('birthYear3').value;
			
			if ( birthMonth3 == 0 || birthDay3 == "" || birthYear3 == "" || birthYear3 == null ){
			    //console.log("Inside Check");
			    jQuery("#invaliddate3").show();
			    jQuery('#3driv_submit').hide();
			    //return false;
			}else{
				jQuery("#invaliddate3").hide();
				jQuery('#3driv_submit').show();
				document.getElementById("3driv_submit").style.display = 'block';
			}
		});
		// jQuery('#2driv_submit,#whatbdateseconddriv,#birthYear2').on('click change',function(){
		// 	var valueDate = document.getElementById('whatbdateseconddriv').value;
		// 	//console.log(valueDate);
		// 	if ( valueDate == null || valueDate == ''){
		// 	    jQuery("#invaliddate2").show();
		// 	    jQuery('#1driv_submit').hide();
		// 	}else{
		// 		jQuery("#invaliddate2").hide();
		// 		jQuery('#2driv_submit').show();
		// 	}
		// });	
		// jQuery('#3driv_submit,#whatbdatethirddriv,#birthYear3').on('click change',function(){
		// 	var valueDate = document.getElementById('whatbdatethirddriv').value;
		// 	//console.log(valueDate);
		// 	if ( valueDate == null || valueDate == ''){
		// 	    jQuery("#invaliddate3").show();
		// 	    jQuery('#3driv_submit').hide();
		// 	}else{
		// 		jQuery("#invaliddate3").hide();
		// 		jQuery('#3driv_submit').show();
		// 	}
		// });	
			

		//Phone number Validation
		jQuery("#submitdata").prop('disabled', true);
		jQuery('#phone').on("keyup change", function(e) {		

	        console.log(this.value.length);
	        if (this.value.length >= 14) {
	            jQuery('#spnPhoneStatus').html('Valid');
	            jQuery('#spnPhoneStatus').css('color', 'green');
	            jQuery("#submitdata").prop('disabled', false);            
	            jQuery('#pnumbersubmit').show();
	        }
	        else {
	            jQuery('#spnPhoneStatus').html('Invalid');
	            jQuery('#spnPhoneStatus').css('color', 'red');
	            jQuery("#submitdata").prop('disabled', true);
	            jQuery('#pnumbersubmit').hide();
	        }
	    });

		//Click on number
		jQuery("#pnumbersubmit" ).click(function() {
			console.log("chal ja bhai");
			//Call ajax
            var universal_leadid = jQuery("#leadid_token").val();
			//var zipcode = jQuery("#zipcode").val();
			var zipcode = document.getElementById("zipcode").value; 
			var auto1year = jQuery('input[name="auto1-year"]:checked').val();
			var auto1make = jQuery('input[name="make"]:checked').val();
			var auto1model = jQuery('input[name="model"]:checked').val();
			var auto1coverageType = jQuery('input[name="coverageType"]:checked').val();
			var pnumber = jQuery("#phone").val();
			var usertimezone = jQuery("#usertimezone").val();

			jQuery.post(
			    my_ajax_object.ajax_url, 
			    {
			        'action': 'foobar',
			        'foobar_id': '786',
			        'universal_leadid':   universal_leadid,
			        'zipcode':   zipcode,
			        'usertimezone':   usertimezone,
			        'auto1year':   auto1year,
			        'auto1make':   auto1make,
			        'auto1model':   auto1model,
			        'auto1coverageType':   auto1coverageType,
			        'pnumber':   pnumber
			    }, 
			    function(response) {
			        console.log('The server responded: ', response);
			    }
			);
		});


			jQuery('#phone').usPhoneFormat({
			    format:'(xxx) xxx-xxxx'
			  });
			function validatePhone() {
			    var a = jQuery("#phone").val();
			    //var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
			    var filter = /^\d{10}$/;
			    if (filter.test(a)) {
			        return true;
			    }
			    else {
			        return false;
			    }
			}
	//Get another Quote
	jQuery("#getotherq").click(function(){
		//jQuery(".insurance-form.form-main").show();
		//location.reload();
		var pageURL = $(location).attr("href");
		window.location.href= pageURL;
	});
	
   //Get city and state form zipcode
   	var apikey = 'AIzaSyAUNWfq7jUprmdjqsEIgnUaTtGbmjgfYXw';
    var geocoder = new google.maps.Geocoder;
    jQuery('#zipcode').bind('change focusout keyup', function () {
        ////console.log("zipcode working.");
        var zipcode = jQuery(this).val();
        //alert($this.val());
            if (jQuery(this).val().length == 5) {
            	jQuery("#step1_submit").prop('disabled', false);
		        geocoder.geocode({ 'address': jQuery(this).val() }, function (result, status) {
		            var state = "N/A";
		            var city = "N/A";
		            //start loop to get state from zip
		            for (var component in result[0]['address_components']) {
		                for (var i in result[0]['address_components'][component]['types']) {
		                    if (result[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
		                        state = result[0]['address_components'][component]['short_name'];
		                        // do stuff with the state here!
		                        //$this.closest('tr').find('select').val(state);
		                        ////console.log("state.."+state);

		                        // get city name
		                        city = result[0]['address_components'][1]['long_name'];
		                        // Insert city name into some input box
		                        ////console.log("city.."+city);
		                        jQuery("#city").html(city);
		                    }
		                }
		            }
		        });
		    }else{
		    	jQuery("#step1_submit").prop('disabled', true);
		    }
	});

		
	});